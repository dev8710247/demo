export interface BookModel {
    code: string;
    author: string;
    title: string;
    type: TextTypes;
    publisher: string;
    edition: number;
}

export enum TextType {
    NOVEL = "novel",
    COLLECTION = "collection",
    NON_FICTION = "non-fiction",
}

export type TextTypes = "novel" | "collection" | "non-fiction";

export interface BooksDataResponseModel {
    booksData: BookModel[];
}