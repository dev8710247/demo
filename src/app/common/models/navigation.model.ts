export interface NavLinkItemModel {
    title: string;
    link: string;
    active: boolean;
    icon?: string;
}