import {provideRouter, Routes} from "@angular/router";
import {HomePageComponent} from "./components/home-page/home-page.component";
import {AboutPageComponent} from "./components/about-page/about-page.component";
import {NotFoundPageComponent} from "./components/not-found-page/not-found-page.component";
import {ApplicationConfig} from "@angular/platform-browser";

const appRoutes: Routes =[
    { path: "", component: HomePageComponent },
    { path: "about", component: AboutPageComponent },
    { path: "**", component: NotFoundPageComponent }
    // redirect examples
    // { path: "contact", redirectTo: "/about", pathMatch:"full"},
    // { path: "**", redirectTo: "/"}
];

export const appConfig: ApplicationConfig = {
    providers: [provideRouter(appRoutes)]
};