import {NgModule} from "@angular/core";
import {AppComponent} from "./app.component";
import {AboutPageModule} from "./components/about-page/about-page.module";
import {HomePageModule} from "./components/home-page/home-page.module";
import {NotFoundPageModule} from "./components/not-found-page/not-found-page.module";
import {RouterModule} from "@angular/router";
import {MatButtonModule} from "@angular/material/button";
import {NavigationModule} from "./components/navigation/navigation.module";
import {CommonModule} from "@angular/common";

@NgModule({
    imports: [
        CommonModule,
        AboutPageModule,
        HomePageModule,
        NotFoundPageModule,
        NavigationModule,
        RouterModule,
        MatButtonModule,
    ],
    declarations: [
        AppComponent,
    ],
    bootstrap: [
        AppComponent,
    ],
})
export class AppModule {
}