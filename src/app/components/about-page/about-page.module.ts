import {NgModule} from "@angular/core";
import {AboutPageComponent} from "./about-page.component";

@NgModule({
    imports: [],
    declarations: [
        AboutPageComponent,
    ],
    exports: [],
    bootstrap: [],
    providers: [],
})
export class AboutPageModule {
}