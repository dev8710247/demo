import {Component} from "@angular/core";

@Component({
    selector: "about-page",
    template: "<h2>About application</h2>"
})
export class AboutPageComponent { }