import {NgModule} from "@angular/core";
import {NotFoundPageComponent} from "./not-found-page.component";

@NgModule({
    imports: [],
    declarations: [
        NotFoundPageComponent,
    ],
    exports: [],
    bootstrap: [],
    providers: [],
})
export class NotFoundPageModule {
}