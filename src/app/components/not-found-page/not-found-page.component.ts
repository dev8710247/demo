import {Component} from "@angular/core";

@Component({
    selector: "not-found-page",
    template: "<h2>Page Not Found</h2>"
})
export class NotFoundPageComponent { }