import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {HomePageComponent} from "./home-page.component";
import {HttpClientModule} from "@angular/common/http";
import {HttpService} from "../../common/srvices/http.service";

@NgModule({
    imports: [
        CommonModule,
        HttpClientModule,
    ],
    declarations: [
        HomePageComponent,
    ],
    exports: [
        HomePageComponent
    ],
    bootstrap: [],
    providers: [
        HttpService,
    ],
})
export class HomePageModule {
}