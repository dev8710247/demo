import {
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    OnDestroy,
    OnInit,
    ViewEncapsulation
} from "@angular/core";
import {BookModel, BooksDataResponseModel} from "../../common/models/book.model";
import {HttpService} from "../../common/srvices/http.service";
import {Subject, takeUntil} from "rxjs";

@Component({
    selector: "home-page",
    templateUrl: "./home-page.component.html",
    styleUrls: ["./home-page.component.less"],
    changeDetection: ChangeDetectionStrategy.OnPush,
    encapsulation: ViewEncapsulation.None,
})
export class HomePageComponent implements OnInit, OnDestroy {

    public _bookItems: BookModel[] = [];

    private destroy$: Subject<void> = new Subject();

    constructor(private httpService: HttpService,
                private cdr: ChangeDetectorRef) {
    }

    ngOnInit(): void {
        this.httpService.getBooksData()
            .pipe(takeUntil(this.destroy$))
            .subscribe((result: BooksDataResponseModel) => {
                this._bookItems = result.booksData;
                this.cdr.detectChanges();
            });
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }
}