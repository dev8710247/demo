import {ChangeDetectionStrategy, Component, HostBinding, OnInit, ViewEncapsulation} from "@angular/core";
import {NavLinkItemModel} from "../../common/models/navigation.model";

@Component({
    selector: "navigation",
    templateUrl: "./navigation.component.html",
    styleUrls: ["./navigation.component.less"],
    changeDetection: ChangeDetectionStrategy.OnPush,
    encapsulation: ViewEncapsulation.None,
})
export class NavigationComponent implements OnInit {
    @HostBinding("class.navigation") readonly mainClass: boolean = true;

    public _navItems: NavLinkItemModel[] = [];

    ngOnInit(): void {
        this._navItems = require("./navigation-items.json") as NavLinkItemModel[];
    }
}