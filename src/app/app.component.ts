import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, ViewEncapsulation} from "@angular/core";
import {RouterOutlet} from "@angular/router";
import {NavigationModule} from "./components/navigation/navigation.module";
import {CommonModule} from "@angular/common";
import {GIT_REPOSITORY_LINK} from "./common/constants/app.constans";
import {MatButtonModule} from "@angular/material/button";
import {HomePageModule} from "./components/home-page/home-page.module";

@Component({
    selector: "app-component",
    standalone: true,
    imports: [
        CommonModule,
        RouterOutlet,
        HomePageModule,
        NavigationModule,
        MatButtonModule,
    ],
    templateUrl: "./app.component.html",
    styleUrls: ["./app.component.less"],
    changeDetection: ChangeDetectionStrategy.OnPush,
    encapsulation: ViewEncapsulation.None,
})
export class AppComponent implements OnInit {
    public _loading: boolean = true;

    constructor(private cdr: ChangeDetectorRef) {
    }

    ngOnInit(): void {
        this.setLoadingTimeout();
    }

    public _copyGitLink(): void {
        navigator.clipboard.writeText(GIT_REPOSITORY_LINK).then(function() {
            console.log("Copying to clipboard was successful!");
        });
    }

    private setLoadingTimeout(): void {
        setTimeout(() => {
            this._loading = false;
            this.cdr.detectChanges();
        }, 1000);
    }
}